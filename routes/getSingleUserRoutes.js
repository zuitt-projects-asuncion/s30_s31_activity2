const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/getSingleUserController");

/*//Create a new user
router.post('/', userControllers.createUserController);*/

//GET all users documents from our users collection
router.get('/getSingleUser/:id',userControllers.getAllUsersController);

module.exports = router;