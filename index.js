// Note: all packages/ modules should be required at the top of the file to avoid tampering or errors

const express = require('express');

// Mongoose is a package that uses an ODM or object document mapper. It allows us to translate our JS objects into database documents for MongoDB. It allows connection and easier manipulation of our documents in MongoDB
const mongoose = require('mongoose');

const taskRoutes = require('./routes/taskRoutes');
const userRoutes = require('./routes/userRoutes');
const singleUserRoutes = require('./routes/singleUserRoutes');
/*const getSingleUserRoutes = require('./routes/getSingleUserRoutes');*/
const port = 4000;

const app = express();

/*
	mongoose.connect is the method to connect your API to your mongoDB via the use of mongoose. It has 2 arguments:

	>> First, is the connection string to connect our api to our mongodb atlas. 

	>> Second, is an object used to add information between mongoose and mongodb.

	>> replace/change <password> in the connection string to your db password

	>> replace/change myFirstDatabase to task169

	MongoDB upon connection and creating our first documents will create the task169 database for us.


	Syntax:
		mongoose.connect("<connectionStringFromMongoDBAtlas>", {
			useNewUrlParser: true,
			useUnifiedTopology: true
		})
*/

mongoose.connect("mongodb+srv://admin_asuncion:admin@cluster0.auukp.mongodb.net/task169?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true

});


let db = mongoose.connection;


db.on('error', console.error.bind(console, 'Connection Error'));


db.once('open', () => console.log('Connected to MongoDB'));

// middlewares
app.use(express.json());


app.use('/tasks', taskRoutes)
app.use('/users', userRoutes)
app.use('/:id', singleUserRoutes)
/*app.use('/getSingleUserRoutes/:id', getSingleUserRoutes)*/

app.listen(port, () => console.log(`Server is running at port ${port}`));
