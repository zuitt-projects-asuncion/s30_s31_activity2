//import the User model:
const singleUser = require("../models/User");

module.exports.getAllUsersController = (req,res) => {

	//Model.find() is a method from our model. This will allow us to find documents from the collection that the model is connected with.
	//mongoDB this may look like this: db.users.find()
	//If the find() method is left with an empty criteria {}, what documents are we getting?
	//All documents in the collection will returned.
	//then() will send all the documents in the collection to the client.
	//catch() will catch any error that may occur while searching for documents.
	User.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))

};

module.exports.updateUserNameController = (req, res) => {

	console.log(req.params.id);
	console.log(req.body);

/*	let updates = {
		 username: req.body.status
	}*/

	let newUser = {
		username: reqBody.username
			      }

	newUser.save()
	User.findByIdAndUpdate(req.params.id, newUser, {new: true})
	.then(result => res.send(result))
	.catch(error => res.send(error))

};